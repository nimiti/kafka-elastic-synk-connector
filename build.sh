#!/bin/bash
set -e

mvn clean install -U -DskipTests=true
cp es-connector/target/yad2-kafka-es-connector-4.1.0-SNAPSHOT-jar-with-dependencies.jar docker
cp jdbc-connector/target/jdbc-connector-4.1.0-SNAPSHOT-jar-with-dependencies.jar docker

cd docker
# Build the docker image using Dockerfile.orig and push it
docker build -t chello/connect .
#docker push ${DOCKER_REGISTRY}/thetaray/kafka-connect-es:${BASE_VERSION}.${BUILD_NUMBER}