#!/bin/bash

pwd=`pwd`
echo $pwd

echo "Starting Kafka Connect application"

bin/connect-distributed.sh config/connect-distributed.properties
